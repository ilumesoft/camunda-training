package de.ilume.training.camunda.volketswil.nonarquillian;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.dmn.engine.DmnDecisionTableResult;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.identityService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.task;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.taskService;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class InMemoryH2Test {

    @ClassRule
    @Rule
    public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

    private static final String PROCESS_DEFINITION_KEY = "camunda-training-01";

    static {
        LogFactory.useSlf4jLogging(); // MyBatis
    }

    @Before
    public void setup() {
        init(rule.getProcessEngine());
    }

    /**
     * Just tests if the process definition is deployable.
     */
    @Test
    @Deployment(resources = {"process.bpmn", "check-tweet.dmn"})
    public void testParsingAndDeployment() {
    }

    @Test
    @Deployment(resources = {"process.bpmn", "check-tweet.dmn"})
    public void testDmnTable(){
        var decisionService = rule.getDecisionService();
        DmnDecisionTableResult decisionResult = decisionService
                .evaluateDecisionTableByKey("check-tweet-item", Variables.createVariables()
                        .putValue("schreiber", "demo")
                        .putValue("content", "Camunda ist doof"));

        Assert.assertEquals(1, decisionResult.getResultList().size());
        Assert.assertEquals(true, decisionResult.getSingleResult().getEntry("tweetApproved"));
    }

    @Test
    @Deployment(resources = {"process.bpmn", "check-tweet.dmn"})
    public void testHappyPath() {
        registerQuickMock("createTweetDelegate", de -> de.setVariable("tweetId", "100"));

        identityService().setAuthenticatedUserId("john");
        var processInstance = runtimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY);

        assertThat(processInstance).isWaitingAt("entwurf-erfassen");
        assertThat(task())
                .isNotNull()
                .isAssignedTo("john");

        taskService().complete(task(processInstance).getId(), Map.of("content", "Mein Tweet, Mein Tweet!"));

        assertThat(processInstance)
                .isEnded()
                .hasPassed("tweet-veroeffentlichen")
                .hasNotPassed("tweet-verwerfen");
    }

    @Test
    @Deployment(resources = {"process.bpmn", "check-tweet.dmn"})
    public void testSadPath() {
        identityService().setAuthenticatedUserId("john");
        var processInstance = runtimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY);
        var externalTaskService = rule.getExternalTaskService();

        registerQuickMock("createTweetDelegate", de -> de.setVariable("tweetId", "100"));




        assertThat(processInstance).isWaitingAt("entwurf-erfassen");
        assertThat(task())
                .isNotNull()
                .isAssignedTo("john");

        taskService().complete(task(processInstance).getId(), Map.of("content", "Camunda ist doof"));

        assertThat(processInstance).isWaitingAt("tweet-verwerfen");

        externalTaskService
                .fetchAndLock(1, "wrkr")
                .topic("tweet-verwerfen", 1000)
                .execute()
                .forEach(task -> externalTaskService.complete(task.getId(), "wrkr"));

        assertThat(processInstance)
                .isEnded()
                .hasPassed("tweet-verwerfen")
                .hasNotPassed("tweet-veroeffentlichen");
    }

    private void registerQuickMock(String name, FunctionalMockDelegate md) {
        Mocks.register(name, md);
    }

}
