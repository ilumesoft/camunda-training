package de.ilume.training.camunda.volketswil;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;

@Named("createTweetDelegate")
public class TweetDelegateMock implements JavaDelegate {

    private final Logger LOGGER = LoggerFactory.getLogger(TweetDelegateMock.class.getName());

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        LOGGER.debug("I am logging the tweetDelegate");
    }

}
