package de.ilume.training.camunda.volketswil;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

@Named("createTweetDelegate")
@ApplicationScoped
public class CreateTweetDelegate implements JavaDelegate {

    private final Logger LOGGER = LoggerFactory.getLogger(CreateTweetDelegate.class.getName());
    private final TwitterService twitterService;

    @Inject
    public CreateTweetDelegate(TwitterService twitterService) {
        this.twitterService = twitterService;
    }

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        String content = getTweetContent(delegateExecution);
        LOGGER.info("Publishing tweet: " + content);
        twitterService.tweet(content);
    }

    private String getTweetContent(DelegateExecution delegateExecution) {
        return (String) delegateExecution.getVariable("content");
    }

}
