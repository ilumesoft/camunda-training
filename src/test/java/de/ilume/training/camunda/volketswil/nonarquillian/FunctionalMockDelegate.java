package de.ilume.training.camunda.volketswil.nonarquillian;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.EmbeddedProcessApplication;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@FunctionalInterface
public interface FunctionalMockDelegate extends JavaDelegate {

    @Override
    void execute(DelegateExecution delegateExecution) throws Exception;
}
