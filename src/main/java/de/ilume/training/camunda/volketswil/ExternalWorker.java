package de.ilume.training.camunda.volketswil;

import org.camunda.bpm.client.ExternalTaskClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class ExternalWorker {

    final static Logger LOG = LoggerFactory.getLogger(ExternalWorker.class.getName());

    public ExternalWorker() {
        LOG.debug("Creating external Task");
    }

    @PostConstruct
    private void startProcessing() {
        ExternalTaskClient client = ExternalTaskClient.create().baseUrl("http://localhost:8080/engine-rest").build();

        client.subscribe("tweet-verwerfen")
                .handler(
                        (externalTask, externalTaskService) -> {
                            LOG.info("Logging the Task, yeah");
                            externalTaskService.complete(externalTask);
                        }).open();

        LOG.info("Completed subscription");
    }
}
