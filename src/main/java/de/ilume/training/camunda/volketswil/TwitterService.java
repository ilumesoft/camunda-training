package de.ilume.training.camunda.volketswil;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import javax.inject.Named;

@Named
public class TwitterService {
    public static final String TOKEN = "220324559-jet1dkzhSOeDWdaclI48z5txJRFLCnLOK45qStvo";
    public static final String TOKEN_SECRET = "B28Ze8VDucBdiE38aVQqTxOyPc7eHunxBVv7XgGim4say";
    public static final String OAUTH_S = "lRhS80iIXXQtm6LM03awjvrvk";
    public static final String OAUTH_S1 = "gabtxwW8lnSL9yQUNdzAfgBOgIMSRqh7MegQs79GlKVWF36qLS";

    public TwitterService() {
    }

    public void tweet(String tweet) throws TwitterException {
        getTwitterConnection().updateStatus(tweet);
    }

    private Twitter getTwitterConnection() {
        AccessToken accessToken = new AccessToken(TOKEN, TOKEN_SECRET);
        Twitter twitter = new TwitterFactory().getInstance();
        twitter.setOAuthConsumer(OAUTH_S, OAUTH_S1);
        twitter.setOAuthAccessToken(accessToken);
        return twitter;
    }
}